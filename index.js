console.log("hello");

// What is Conditional Statements?
	// Conditional Statement allows us to control the flow of our program.
	// It allows us to run statement//instructions if a condition is met or run another separate instruction if not otherwise.

// [SECTION] id, else if and else styatement

let numA = -1;

/*
	if statement
		- it will execute the statement/code blocks if a specified condition is met/true.

*/

if(numA<0){
	console.log("Hello from numA");
}

console.log(numA<0);

/*
	Syntax:
	if(condition){
		statement;
	}
*/
	// The result of the expression added in the if's condition must result to true, else the statement inside the if () will not run.

// let's reassign the variable numA and run an if staement with the same condition.

numA = 1;

if (numA<0){
	console.log("Hello from the reassigned value of numA");
}
console.log(numA<0);

// it will not run because the expression now results to false.

let city = "New York";

if (city === "New York"){
	console.log("Welcome to New York!")
}

// else if clause
/*
	-Execute a statement if previous conditions are false and if the specified condition is true.
	- The 'else if' is optional and can be added to capture additional conditions to change the flow of a program.
*/

let numH = 1;

if (numH<0){
	console.log("Hello from (num<0).");
}
else if(numH>0){
	console.log("Hello from (numH>0)");
}

/* We were able to run the else if () statement after we evaluated the if condition was failed /false */

// if the if() condition was passed and run, we will no longer evaluate to else if() and end the process.

// else if is dependent with if, you cannot use else if clause alone.
/*{
	else if(numH>0){
		console.log("Hello from (numH>0)");
	}

}*/
// numH = 1;
if (numH!==1){
	console.log("Hello from num === 1!");
}

else if (numH===1){
	console.log("Hello from num>0!");
}

else if(numH>0){
	console.log("Hello from the second else if clause!");
}

city = "Tokyo";

if (city === "New York"){
	console.log("Welcome to New York!")
}
else if (city==="Tokyo"){
	console.log("Welcome to Tokyo!")
}

// else statement
	/*
		- Execute a statement if all other conditions if all other conditions are false/not met 
		- else if statement is optional and can be added to capture any other result to change the flow of program.
	*/

num = 2;
// false
if (numH<0){
	console.log("Hello from if statement");
}
// false
else if(numH>2){
	console.log("hello from the first else if");
}
// false
else if(numH>3){
	console.log("hello from the second else if");
}
// since all the conditions above are false/not met, else statement will run
else{
	console.log("hello from the else statement");
}

// Since all of the preceeding if and else conditions were not met, the else statement was executed instead.
// else statement is also dependent with if statement, it cant go alone.

/*{
	else{
		console.log("Hello from the else inside the code block")
	}
}*/

	// if, else if and else staement with functions
	/*
		Most of the times we would like to use if, else if and if statement with functions to control the flow of our application.
	*/

let message;

function determineTyphoonIntensity(windSpeed){
	if(windSpeed<0){
		return "Invalid argument!";
	}
	else if(windSpeed<=30){
		return "Not a typhoon yet!"
	}
	else if(windSpeed<=60){
		return "Tropical depression detected!";
	}
	else if(windSpeed<=88){
		return "Tropical Storm detected!";
	}
	else if(windSpeed<=117){
		return "Sever Tropical Storm detected!";
	}
	else if(windSpeed>=118){
		return "Typhoon Detected!";
	}
	else {
		return "Invalid argument!";
	}


}

	// mini-activity
	// 1. add return tropical storm detected if the windspeed is between 60 and 89
	// 2. add return "Severe Tropical Storm Detected" if the windspeed is between 88 and 118
	//3. if higher than 117, return "Typhoon Detected!"
console.log(determineTyphoonIntensity("q"));

	message = determineTyphoonIntensity(119);

	if (message === "Typhoon detected"){
		// console.warn() is a good way to print warning in our console that could help us developers act on certain output within our code.
		console.warn(message);
	}

// [SECTION] Truthy or falsy
	// Javascript a "truthy" is a value that is considered true when encountered in Boolean context
	// Values are considered true unless defined otherwise.

	// falsy values/ exceptions for truthy
	/*
		1. false
		2. 0
		3. -0
		4. " "
		5. null
		6. undefined
		7. NaN -not a number
	*/

if(true){
	console.log("true")
}
if(1){
	console.log("true")
}
if(null){
	console.log("Hello from null inside the if condition.")
}
else{
	console.log("hello from the else of the null conditrion")
}

// [SECTION] Condition operator/ ternary operator
	/*
		The conditional operator
		1. condition/expression
		2. expression to execute if the condition is true or truthy
		3. expression if the condition is falsy
		Syntax:
		(expression) ? ifTrue : if False;

	*/

	let ternaryResult = (1>18) ? true : false;
	console.log(ternaryResult);


// [SECTION] Switch Statement
	/*
		The Swith statement evaluates an expression and matches the expression's value to a case class.
	*/
	// toLowerCase() method will convert all the letters into small letters.
	let day = prompt("What day of the week today?").toLowerCase();
	switch (day){
		case 'monday' :
			console.log('The color of the day is red!');
			// it means the code will stop here
			break;
		case 'tuesday':
			console.log('The color of the day is orange!');
			break;
		case 'wednesday':
			console.log('The color of the day is yellow!');
			break;
		case 'thursday':
			console.log('The color of the day is green!');
			break;
		case 'friday':
			console.log('The color of the day is blue!');
			break;
		case 'saturday':
			console.log('The color of the day is indigo!');
			break;
		case 'sunday':
			console.log('The color of the day is violet!');
			break;

		default:
			console.log("Please input a valid day!");
			break;	
		
	}

// [SECTION] Try-catch-finally
	// tr-catch statement for error handling
	// there are instances when the application returns an error/warning that is not necessarily an error in the context of our code.
	//  these errors are result of an attempt of programming language to help developers in creating effecient code

	function showIntensity(windSpeed){
		try{
			// codes that will be executed or run
			alerat(determineTyphoonIntensity(windSpeed));
			alert("Intensity updates will show alert from try.")
		}
		// if may error kay try, magra run si catch
		catch(error){
			console.warn(error.message);
		}
		finally{
			// continue execution of code regardless of success and failuer of code execution in the try statement
			alert("Intensity updates will show new alert from finally")
		}
	}

	showIntensity(119);